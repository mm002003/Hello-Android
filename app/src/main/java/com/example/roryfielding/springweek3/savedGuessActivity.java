package com.example.roryfielding.springweek3;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;


/**
 * Created by RoryFielding on 02/02/2018.
 */

public class savedGuessActivity extends AppCompatActivity {


    //UI Elements we want
    private TextView score1;
    private TextView score2;
    private TextView score3;
    private Button scoreBtn1;
    private Button scoreBtn2;
    private Button scoreBtn3;
    Guess guessingGame;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_saved_guess);

        //run our setup function for this class
        Setup();
    }

    /**
     * simple convinience setup function that gets the UI elements we want
     * and loads teh colours from shared preferences for each one
     */
    public void Setup(){

        //get UI elements
        score1=(TextView)findViewById(R.id.TVsc1);
        score2=(TextView)findViewById(R.id.TVsc2);
        score3=(TextView)findViewById(R.id.TVsc3);
        scoreBtn1=(Button)findViewById(R.id.scoreBtn1);
        scoreBtn2=(Button)findViewById(R.id.scoreBtn2);
        scoreBtn3=(Button)findViewById(R.id.scoreBtn3);


        //get the shared prefs
        SharedPreferences sp = getSharedPreferences(
                getResources().getString(R.string.SharedPrefs),
                0);
        //check for the colours
        checkForSavesinSP(sp);

        //set on click listeners for buttons
        scoreBtn1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                backToMain(1);
            }
        });
        scoreBtn2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                backToMain(2);
            }
        });
        scoreBtn3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                backToMain(3);
            }
        });

    }

    /**
     *
     * @param sp
     */
    private void checkForSavesinSP(SharedPreferences sp){

        //we want to check if there are any empty locations, so we will try and
        //retrieve the red value for each one to see if they exist
        // the -1 is what will be returned to us if it doesn't exist
        int r1 = sp.getInt(getResources().getString(R.string.Score_1),-1);
        int r2 = sp.getInt(getResources().getString(R.string.Score_2),-1);
        int r3 = sp.getInt(getResources().getString(R.string.Score_3),-1);


        //temp ints
        int valguess, guessleft, score;

        //we'll look through and see if any are not null
        if(r1!=-1){
            valguess = sp.getInt(getResources().getString(R.string.sc1val2guess),0);
            guessleft = sp.getInt(getResources().getString(R.string.sc1guessleft),0);
            score = sp.getInt(getResources().getString(R.string.Score_1),0);

            guessingGame.valToGuess = valguess;
            guessingGame.guesscounter = guessleft;
            guessingGame.scoretracker = score;

        }

        if(r2!=-1){
            valguess = sp.getInt(getResources().getString(R.string.sc2val2guess),0);
            guessleft = sp.getInt(getResources().getString(R.string.sc2guessleft),0);
            score = sp.getInt(getResources().getString(R.string.Score_2),0);

            guessingGame.valToGuess = valguess;
            guessingGame.guesscounter = guessleft;
            guessingGame.scoretracker = score;
        }

        if(r3!=-1){
            valguess = sp.getInt(getResources().getString(R.string.sc3val2guess),0);
            guessleft = sp.getInt(getResources().getString(R.string.sc3guessleft),0);
            score = sp.getInt(getResources().getString(R.string.Score_3),0);

            guessingGame.valToGuess = valguess;
            guessingGame.guesscounter = guessleft;
            guessingGame.scoretracker = score;
        }


    }

    private void backToMain(int col){

        //create an intent to take use back
        Intent intMain = new Intent(this, MainActivity.class);

        //create an int array to return with the intent
        int[] GUESS = {0,0,0};

        //get the shared prefs
        SharedPreferences sp = getSharedPreferences(
                getResources().getString(R.string.SharedPrefs),
                0);

        //add the prefered colour into the array
        switch(col){
            case 1:
                //set the first buttons colour
                GUESS[0] = sp.getInt(getResources().getString(R.string.sc1val2guess),0);
                GUESS[1] = sp.getInt(getResources().getString(R.string.sc1guessleft),0);
                GUESS[2] = sp.getInt(getResources().getString(R.string.Score_1),0);
                break;
            case 2:
                //set the first buttons colour
                GUESS[0] = sp.getInt(getResources().getString(R.string.sc2val2guess),0);
                GUESS[1] = sp.getInt(getResources().getString(R.string.sc2guessleft),0);
                GUESS[2] = sp.getInt(getResources().getString(R.string.Score_2),0);
                break;
            case 3:
                //set the first buttons colour
                GUESS[0] = sp.getInt(getResources().getString(R.string.sc3val2guess),0);
                GUESS[1] = sp.getInt(getResources().getString(R.string.sc3guessleft),0);
                GUESS[2] = sp.getInt(getResources().getString(R.string.Score_3),0);
                break;
        }

        //attach the int array to the intent
        intMain.putExtra(getResources().getString(R.string.guessLoad),GUESS);

        //start the main activity!
        startActivity(intMain);

    }

}
