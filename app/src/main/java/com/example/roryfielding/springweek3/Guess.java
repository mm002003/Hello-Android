package com.example.roryfielding.springweek3;

import java.util.Random;

/**
 * Created by RoryFielding on 23/01/2018.
 */

public class Guess {

    public int valToGuess;
    private int range;
    public int guesscounter;
    public int scoretracker;

    /**
     * defaults the range to 20 and sets a guess value
     */
    public Guess() {
        range = 20;
        SetNewValue();
        guesscounter = 3;
        scoretracker = 0;
    }

    /**
     * Second constructor sets the range to the input value and then sets value
     *
     * @param rangeIn
     */
    public Guess(int rangeIn) {
        this.range = rangeIn;
        this.guesscounter = range - 2;
        SetNewValue();
    }

    /**
     * SetNewValue uses the java Random class to set the new value to guess between 1 and range
     */
    public void SetNewValue() {

        //create Random object to create our new val
        Random rng = new Random();

        //get random value, I use range -1 and then add 1 on to the generated number to ensure
        //that we get a value between 1 with no chance of getting zero
        valToGuess = 1 + rng.nextInt(range - 1);
        scoretracker = 0;

    }


    public boolean MakeGuess(int userGuess) {

        //assume guess is false to begin
        boolean correct = false;

        //check if guess equals our value and set boolean to true if so
        if (userGuess == valToGuess) {
            scoretracker++;
            correct = true;
        }

        //return the result
        guesscounter--;
        return correct;
    }


}

