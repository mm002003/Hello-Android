package com.example.roryfielding.springweek3;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.util.Random;

public class MainActivity extends AppCompatActivity {

    enum BTNSTATE {GUESS, TRYAGAIN};

    //UI Elements we want to interact with
    Button enterGuessBtn;
    Button saveBtn;
    Button loadBtn;
    TextView instructionTextView;
    TextView resultTextView;
    EditText enterGuessEditText;
    TextView textViewGuessesLeft;
    TextView TV_HighScore;

    //Event Handlers
    View.OnClickListener guessClickListener;
    View.OnClickListener tryAgainClickListener;
    View.OnKeyListener editTextGuessListener;

    //variables we will need to work the game correctly
    Guess guessingGame;
    BTNSTATE curBtnState;


    @Override
    protected void onSaveInstanceState(Bundle savedInstanceState){
        super.onSaveInstanceState(savedInstanceState);
        savedInstanceState.putInt(getResources().getString(R.string.STATE_Left_Guesses), guessingGame.guesscounter);
        savedInstanceState.putInt(getResources().getString(R.string.STATE_VAL), guessingGame.valToGuess);

    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState){
        super.onRestoreInstanceState(savedInstanceState);
        guessingGame.guesscounter = savedInstanceState.getInt(getResources().getString(R.string.STATE_Left_Guesses));
        guessingGame.valToGuess = savedInstanceState.getInt(getResources().getString(R.string.STATE_VAL));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        //run setup
        Setup();


    }

    /**
     * Convenience function for setup (as we may need to call on it from multiple points in the app
     * lifetime
     */
    private void Setup(){

        //get our UI elements
        enterGuessBtn = (Button)findViewById(R.id.GuessBtn);
        saveBtn= (Button)findViewById(R.id.saveBtn);
        loadBtn = (Button)findViewById(R.id.loadBtn);
        resultTextView = (TextView)findViewById(R.id.TV_result);
        enterGuessEditText = (EditText)findViewById(R.id.ET_guess_number);
        instructionTextView = (TextView)findViewById(R.id.TV_instruction);
        textViewGuessesLeft = (TextView) findViewById(R.id.Textviewguessesleft);
        TV_HighScore = (TextView) findViewById(R.id.TV_HighScore);

        // make sure our button is initialised to the GUESS state
        SetButtonState(BTNSTATE.GUESS);

        saveBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //call a function to save the current colour to shared prefs
                SaveGuessToSP();
            }
        });

        loadBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //call funtion to load saved colours activity
                LoadSavedGuessAct();
            }
        });

        //declare our guess click listener - this will check to see if the user has guessed
        //correctly
        guessClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MakeGuess();
            }
        };

        //Try again click listener will reset the game with a new value and reset the result text to
        //the starting value
        tryAgainClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //Set result to blank (i've used a dash but you could set it to whatever)
                resultTextView.setText("-");

                //Set a new value for the guessing game
                guessingGame.SetNewValue();

                //set highscore
                TV_HighScore.setText(getResources().getString(R.string.Score1,guessingGame.scoretracker));
                //Then change the button state back to guess
                SetButtonState(BTNSTATE.GUESS);

            }
        };

        //Guess key listener is to try and get the enter button from the keyboard to
        // make the guess rather than having to input and then press the button
        editTextGuessListener = new View.OnKeyListener() {
            @Override
            public boolean onKey(View view, int keyCode, KeyEvent event) {
                //only attempt guess if we are in guess state
                if(curBtnState==BTNSTATE.GUESS) {
                    //only check for enter on key down
                    if (event.getAction() == KeyEvent.ACTION_DOWN) {

                        //depending on key pressed do...
                        switch (keyCode) {
                            case KeyEvent.KEYCODE_DPAD_CENTER:
                            case KeyEvent.KEYCODE_ENTER:
                                MakeGuess();
                                return true;
                            default:
                                break;
                        }
                    }
                }
                return false;
            }
        };

        //set key event listener for edittext
        enterGuessEditText.setOnKeyListener(editTextGuessListener);

        //initialise the Guess object, we need to get our range value from the integers.xml file
        // this is the same as with getString - we just provide the ID of our value
        int range = getResources().getInteger(R.integer.range);
        guessingGame = new Guess(range);

        //because I have a variable in my text to set I need to change the text of the instructions
        // so that it includes our range (so that %d in the string shows the actual value!
        //the getString function will automatically put the value in for us when we call it
        // like in the code below
        instructionTextView.setText(getResources().getString(R.string.Instructions,range));
        textViewGuessesLeft.setText(getResources().getString(R.string.Guesses_left,guessingGame.guesscounter));

        //set the result text to blank
        resultTextView.setText("-");
    }

    /**
     * Function that we can call when a guess is made by clicking a button
     */
    public void MakeGuess(){

        //Get our Guess value from the edit text by getting the input text and
        //casting the number from the string
        int guessVal = Integer.parseInt(enterGuessEditText.getText().toString());

        //get our range value again so we can use it later
        int range = getResources().getInteger(R.integer.range);

        //remove the text from the edit text so that the user knows that the guess has
        //been input
        enterGuessEditText.setText("");

        //only make the guess if the value is in range
        if(guessVal>0 && guessVal<=range) {

            //pass the guess to the Guess object and change the result text
            //and button state based on the result
            if (guessingGame.MakeGuess(guessVal)) {

                //user was correct so set the result text to the win condition
                resultTextView.setText(R.string.result_win);
                TV_HighScore.setText(getResources().getString(R.string.Score1,guessingGame.scoretracker));
                guessingGame.guesscounter = 3;

                //set the button state to try again so user can reset the game
                SetButtonState(BTNSTATE.TRYAGAIN);

            } else {

                //otherwise user was incorrect so set the result text
                resultTextView.setText(R.string.result_lose);
                textViewGuessesLeft.setText(getResources().getString(R.string.Guesses_left,guessingGame.guesscounter));

            }
        }else{
            //if the guess is not within range we need to set the result text to let
            //the user know that the guess was invalid - again the out of range string
            //that was declared has a variable that needs to be filled so rather than just
            //passing in the ID we need to pass the value for that as well!
            resultTextView.setText(getResources().getString(R.string.result_out_of_range,range));
        }

    }

    /**
     * Function used to change the state of the button - whether it should be showing the
     * user Guess or Try Again depending on whether they have succeeded in the game or not
     */
    private void SetButtonState(BTNSTATE newState){

        //set the current button state variable
        curBtnState = newState;

        //all we are doing here is setting what the button should say and what action it
        //should perform based on which state it is being set to
        if(newState==BTNSTATE.GUESS){
            //set the button text
            enterGuessBtn.setText(R.string.button_guess);
            //set the click listener
            enterGuessBtn.setOnClickListener(guessClickListener);
        }else{
            //set the button text
            enterGuessBtn.setText(R.string.button_try_again);
            //set the try again click listener
            enterGuessBtn.setOnClickListener(tryAgainClickListener);
        }
    }

    /**
     * This function handles saving a colour to shared preferences for us
     */
    private void SaveGuessToSP(){

        //get the shared preferences
        SharedPreferences sp = getSharedPreferences(
                getResources().getString(R.string.SharedPrefs),
                0 );
        //we will also get the editor so that we can write to it!
        SharedPreferences.Editor spEdit = sp.edit();

        //we want to check if there are any empty locations, so we will try and
        // the -1 is what will be returned to us if it doesn't exist
        int r1 = sp.getInt(getResources().getString(R.string.Score_1),-1);
        int r2 = sp.getInt(getResources().getString(R.string.Score_2),-1);
        int r3 = sp.getInt(getResources().getString(R.string.Score_3),-1);
        //we will store which one we are choosing in int
        int choice = -1;

        //we'll look through and see if any are free
        if(r1==-1){
            //write the values to 1
            choice=1;
        }else if(r2==-1){
            //write the values to 2
            choice=2;
        }else if(r3==-1){
            //write the values to 3
            choice=3;
        }else{
            //pick one randomly
            Random rng=new Random();
            choice= rng.nextInt(2)+1;//rng can be zero so we add 1
        }

        //now we will write out to one of the scores based on our score values that exist
        switch(choice){
            case 1:
                spEdit.putInt(getResources().getString(R.string.Score_1), guessingGame.scoretracker);
                spEdit.putInt(getResources().getString(R.string.sc1val2guess),guessingGame.valToGuess);
                spEdit.putInt(getResources().getString(R.string.sc1guessleft),guessingGame.guesscounter);
                break;
            case 2:
                spEdit.putInt(getResources().getString(R.string.Score_2),guessingGame.scoretracker);
                spEdit.putInt(getResources().getString(R.string.sc2val2guess),guessingGame.valToGuess);
                spEdit.putInt(getResources().getString(R.string.sc2guessleft),guessingGame.guesscounter);
                break;
            case 3:
                spEdit.putInt(getResources().getString(R.string.Score_3), guessingGame.scoretracker);
                spEdit.putInt(getResources().getString(R.string.sc3val2guess),guessingGame.valToGuess);
                spEdit.putInt(getResources().getString(R.string.sc3guessleft), guessingGame.guesscounter);
        }

        //commit the changes otherwise it wont actually write to the file!!
        spEdit.commit();
    }

    /**
     * load the saved colours activity
     *
     */
    private void LoadSavedGuessAct(){
        //create the intent - passing this and the class of the activity we want to load
        Intent lscInt = new Intent(this, savedGuessActivity.class);
        //then just pass the Intent to start activity
        startActivity(lscInt);
    }



}
